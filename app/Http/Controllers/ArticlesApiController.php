<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateArticleRequest;
use App\Articles\Article;

class ArticlesApiController extends Controller
{
    /**
     * @param CreateArticleRequest $request
     */
    public function store(CreateArticleRequest $request) {
        return Article::create($request->all(), 201);
    }
}
