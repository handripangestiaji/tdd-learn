<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Exceptions\StreakApiException;

class StreakApiController extends Controller
{
    private $client;
    private $api_key;
    private $base_uri;

    public function __construct(Client $client, $base_uri, $api_key = null)
    {
        $this->client = $client;
        $this->api_key = $api_key;
        $this->base_uri = $base_uri;
    }

    private function throwException($message, $code = 400)
    {
        throw new StreakApiException($message, $code);
    }

    /**
     * Method to call team in streak api
     * 
     * @return JSON
     */
    public function getTeam()
    {
        if(!$this->api_key) {
            $this->throwException('Api key is required');
        }

        try {
            $response = $this->client->request('GET', $this->base_uri.'v2/users/me/teams', [
                'auth' => [$this->api_key, null]
            ]);

            $contents = $response->getBody();

            return $contents;
        } catch (ClientException $e) {
            $this->throwException(sprintf('Failed to get team data'));
        }
    }
}
