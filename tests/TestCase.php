<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Http\Controllers\StreakApiController;
use GuzzleHttp\Client;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Set up the test
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Reset the migrations
     */
    public function tearDown()
    {
        parent::tearDown();
    }
}
