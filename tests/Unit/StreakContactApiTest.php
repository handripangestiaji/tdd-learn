<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Controllers\StreakApiController;
use App\Exceptions\StreakApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class StreakContactApiTest extends TestCase
{
    public function testShouldThrowExceptionForEmptyApiKeyArguments()
    {
        $this->expectException(StreakApiException::class);
        $this->expectExceptionMessage('Api key is required');
        $this->expectExceptionCode(400);

        $streak = $this->getStreak(400, null, null);
        
        $streak->getTeam();
    }

    public function testShouldThrowExceptionForInvalidApiKeyArguments()
    {
        $api_key = 'thisisinvalid';

        $this->expectException(StreakApiException::class);
        $this->expectExceptionMessage(sprintf('Failed to get team data'));
        $this->expectExceptionCode(400);

        $streak = $this->getStreak(400, null, $api_key);

        $streak->getTeam();

    }

    public function testShouldReturnData()
    {
        $api_key = env('STREAK_API_KEY');

        $body = file_get_contents(__DIR__.'/response-body.txt');

        $streak = $this->getStreak(200, $body, $api_key);

        $result = $streak->getTeam();

        $this->assertEquals($body, $result);
    }

    private function getStreak($status, $body = null, $api_key) 
    {
        $mock = new MockHandler([new Response($status, [], $body)]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        return new StreakApiController($client, 'http://mocked.streak.xyz', $api_key);
    }
}
